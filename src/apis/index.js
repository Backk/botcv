// const BASE_URL = "http://localhost:3333/api/v1";
const BASE_URL = "https://shiny-fly-sweatshirt.cyclic.app/api/v1";

const LOGIN_URL = BASE_URL + "/auth/login";
const REGISTER_URL = BASE_URL + "/auth/register";
const CREATE_POST = BASE_URL + "/post/create";
const GET_POST_BY_ID = BASE_URL + "/post/get-post";
const GET_POSTS = BASE_URL + "/post/get-my-post";
const SEND_CV = BASE_URL + "/post/add-cv";
const SEND_EMAIL = BASE_URL + "/email";
const GET_ALL_POST = BASE_URL + "/post";

export default {
  LOGIN_URL,
  REGISTER_URL,
  CREATE_POST,
  GET_POST_BY_ID,
  GET_POSTS,
  SEND_CV,
  SEND_EMAIL,
  GET_ALL_POST,
};
