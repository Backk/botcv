import LoginImage from "./login.png";
import Logo from "./logo.png";
import User from "./user.png";
import HR from "./hr.png";
import LoginImage1 from "./login2.jpg";
import Home from "./home1.png";
import HomeBackground from "./homeBackground.png";
import Mau1 from "./mau1.png";
const images = {
  LoginImage,
  Home,
  Logo,
  User,
  HR,
  HomeBackground,
  LoginImage1,
  Mau1,
};
export default images;
