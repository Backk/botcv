import React from "react";
import { Outlet } from "react-router-dom";
import AppHeader from "./layouts/components/AppHeader/AppHeader";
import AppFooter from "./layouts/components/AppFooter/footer";

function App() {
  // return <AuthContextProvider>

  // </AuthContextProvider>;
  return (
    <div>
      <AppHeader />
      <div
        style={{
          paddingTop: "78px",
          backgroundColor: "#F0F0F0",
        }}
      >
        <Outlet />
      </div>
      <div
        style={{ height: "30px", width: "100%", backgroundColor: "#f0f0f0" }}
      ></div>
      <div
        style={{
          display: "block",
          backgroundColor: "#f0f0f0",
        }}
      >
        <AppFooter />
      </div>
    </div>
  );
}

export default App;
