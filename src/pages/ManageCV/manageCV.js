import styles from "./style.module.css";
import AddIcon from "@mui/icons-material/Add";
import images from "../../assets/images/images";
import { useContext, useState } from "react";
import { AuthContext } from "../../context/authContext";
import Radio from "@mui/material/Radio";
import FormControlLabel from "@mui/material/FormControlLabel";
import RadioGroup from "@mui/material/RadioGroup";
function ManageCV() {
  const [value, setValue] = useState("");
  const [isOpened, setIsOpened] = useState(false);
  function toggle() {
    setIsOpened((wasOpened) => !wasOpened);
  }
  const [isOpened1, setIsOpened1] = useState(false);
  function navtoggle() {
    setIsOpened1((wasOpened1) => !wasOpened1);
  }
  const handleRadioChange = (event) => {
    setValue(event.target.value);
  };
  const ListCV = [
    {
      name: "CV đã tạo trên botCV",
      img: "https://www.topcv.vn/v4/image/cv-manager/no-cv.png",
      content: "Bạn chưa tạo CV nào",
    },
    {
      name: "CV đã tải lên botCV",
      img: "https://www.topcv.vn/v4/image/cv-manager/no-cv-upload.png",
      content: "Bạn chưa tải lên CV nào",
    },
    {
      name: "BotCV Profile",
      img: "https://www.topcv.vn/v4/image/cv-manager/no-profile.png",
      content: "Bạn chưa tạo BotCV Profile",
    },
  ];
  const {
    authState: { isAuthenticated, user },
    logout,
  } = useContext(AuthContext);

  return (
    <div className={styles.container}>
      <div>
        {ListCV.map((Lists, index) => {
          return (
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                width: "100%",
              }}
            >
              <div className={styles.cvList}>
                <div className={styles.boxHeader}>
                  <h2>{Lists.name}</h2>
                  <div>
                    <a
                      href="http://localhost:3000/ho-so-va-cong-viec"
                      className={styles.buttonAdd}
                    >
                      <AddIcon />
                      Tạo Mới
                    </a>
                  </div>
                </div>
                <div className={styles.contentBox}>
                  <img src={Lists.img}></img>
                  <p>{Lists.content}</p>
                </div>
              </div>
            </div>
          );
        })}
      </div>
      <div>
        <div className={styles.account}>
          <div className={styles.userHeader}>
            <div>
              <img src={images.User} className={styles.user} />
              <div
                style={{
                  fontSize: 11,
                  cursor: "pointer",
                  color: "#777",
                  marginLeft: 10,
                  fontStyle: "italic",
                }}
              >
                Cập nhật ảnh
              </div>
            </div>
            <div>
              <div style={{ fontSize: 14, marginTop: 10, marginLeft: 15 }}>
                Chào bạn trở lại,
              </div>
              <p style={{ marginLeft: 15, fontSize: "18px" }}>
                {user && user.email}
              </p>
            </div>
          </div>
          <div>
            <div style={{ display: "flex", flexDirection: "row" }}>
              <div
                className={styles.button}
                onClick={toggle}
                style={{
                  backgroundColor: isOpened ? "rgb(0, 177, 79)" : "#f0f0f0",
                }}
              >
                <div
                  className={styles.navButton}
                  style={{
                    left: isOpened ? 22 : 0,
                    backgroundColor: isOpened
                      ? "rgb(255, 255, 255)"
                      : "#ff3737",
                    transition: "All 0.5s",
                  }}
                ></div>
              </div>
              <h2
                style={{
                  fontSize: 15,
                  color: isOpened ? "rgb(0, 177, 79)" : "#d0011b",
                }}
              >
                {isOpened && <div>Trạng thái tìm việc đang bật</div>}
                {!isOpened && <div>Trạng thái tìm việc đang tắt</div>}
              </h2>
            </div>
            {isOpened && (
              <div className={styles.fontNavButton}>
                Nhà tuyển dụng trên BotCV sẽ chủ động liên hệ với bạn, hãy sẵn
                sàng nghe điện thoại để nhận cơ hội tốt.
              </div>
            )}
            {!isOpened && (
              <div className={styles.fontNavButton}>
                Bật tìm việc để nhận được nhiều cơ hội việc làm tốt nhất từ
                BotCV
              </div>
            )}
          </div>
          <div style={{ display: "flex", flexDirection: "row" }}>
            <div
              className={styles.button}
              onClick={navtoggle}
              style={{
                backgroundColor: isOpened1 ? "rgb(0, 177, 79)" : "#f0f0f0",
              }}
            >
              <div
                className={styles.navButton}
                style={{
                  left: isOpened1 ? 22 : 0,
                  backgroundColor: isOpened1 ? "rgb(255, 255, 255)" : "#ff3737",
                  transition: "All 0.5s",
                }}
              ></div>
            </div>
            <h2
              style={{
                fontSize: 15,
                color: isOpened ? "rgb(0, 177, 79)" : "#d0011b",
              }}
            >
              {isOpened1 && (
                <div style={{ color: isOpened1 ? "#00b14f" : "#d0011b" }}>
                  Cho phép NTD liên hệ bạn qua
                </div>
              )}
              {!isOpened1 && <div>Cho phép NTD liên hệ bạn qua</div>}
            </h2>
          </div>
          <div>
            {isOpened1 && (
              <div>
                <RadioGroup
                  aria-labelledby="demo-error-radios"
                  name="button"
                  value={value}
                  onChange={handleRadioChange}
                >
                  <div className={styles.radio}>
                    <FormControlLabel
                      value="CV Online"
                      control={
                        <Radio style={{ color: "#2db14f" }} size="small" />
                      }
                      label="CV Online"
                    />
                    <FormControlLabel
                      value="Hồ sơ BotCV"
                      control={
                        <Radio style={{ color: "#2db14f" }} size="small" />
                      }
                      label="Hồ sơ BotCV"
                      sx={{ fontWeight: 10 }}
                    />
                  </div>
                </RadioGroup>

                <div className={styles.fontNavButton}>
                  Cho phép các Nhà tuyển dụng đã được BotCV xác thực xem CV
                  Online để có thể liên hệ với bạn
                </div>
              </div>
            )}
            {!isOpened1 && (
              <div className={styles.fontNavButton}>
                Bật để cho phép các đơn vị tuyển dụng uy tín, Headhunter đã được
                BotCV xác thực xem CV Online của bạn.
              </div>
            )}
          </div>
          <div className={styles.profileCV}>
            Khởi tạo BotCV Profile để gia tăng 300% cơ hội việc làm tốt
          </div>
          <div className={styles.buttonContent} style={{marginLeft: 10, cursor: "pointer"}}>Tạo BotCV Profile</div>
        </div>
        <div className={styles.footer}>
          <h3>CV của bạn đã đủ tốt?</h3>
          <p>Bao nhiêu NTD đang quan tâm tới Hồ sơ của bạn?</p>
          <div
            style={{
              display: "flex",
              flexDirextion: "row",
              justifyContent: "space-between",
            }}
          >
            <div className={styles.countView}>
              <h1>0</h1>
              <p>lượt</p>
            </div>
            <div className={styles.contentCount}>
              <span className={styles.content}>
                Mỗi lượt Nhà tuyển dụng xem CV mang đến một cơ hội để bạn gần
                hơn với công việc phù hợp.
              </span>
              <div className={styles.buttonContent}>Khám phá ngay</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default ManageCV;
