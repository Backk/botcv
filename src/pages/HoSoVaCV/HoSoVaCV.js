import styles from "./style.module.css";
import Grid from "@mui/material/Grid";
import VisibilityIcon from "@mui/icons-material/Visibility";
import ModeIcon from "@mui/icons-material/Mode";
import { template } from "./Template";
function HoSoVaCV() {
  return (
    <div style={{ paddingLeft: 100, paddingRight: 100 }}>
      <div>
        <h2 style={{ color: "#00b14f" }}>
          Danh sách mẫu CV xin việc chuẩn 2022
        </h2>
        <p style={{ color: "#666" }}>
          Các mẫu CV được thiết kế theo chuẩn, theo ngành nghề. Phù hợp với sinh
          viên và người đi làm.
        </p>
        <h3>Mẫu CV thông thường</h3>
      </div>
      <div>
        <Grid container spacing={5}>
          {template.map((templates, index) => {
            return (
              <Grid item md={4} sm={6}>
                <div
                  style={{
                    // marginLeft: "12.5%",
                    marginTop: 10,
                    // marginRight: "12.5%",
                  }}
                >
                  <div className={styles.templateImage}>
                    <div className={styles.navtemplateImage}>
                      <div className={styles.previewButton}>
                        <VisibilityIcon className={styles.Icon} />
                        <span style={{ color: "white" }}>Xem trước</span>
                      </div>
                      <div className={styles.createCV}>
                        <ModeIcon className={styles.Icon} />
                        <span style={{ color: "white" }}>Dùng mẫu này</span>
                      </div>
                    </div>
                    <img width={"100%"} src={templates.img}></img>
                  </div>
                  <div className={styles.templateItemInfo}>
                    <div className={styles.templateMainItem}>
                      <a
                        href="https://www.topcv.vn/mau-cv-chuyen-nghiep"
                        className={styles.templateItem}
                      >
                        {templates.item1}
                      </a>
                      <a
                        href="https://www.topcv.vn/mau-cv-sang-tao"
                        className={styles.templateItem}
                      >
                        {templates.item2}
                      </a>
                    </div>
                    <p className={styles.nameTemplate}>{templates.name}</p>
                    <div className={styles.templateColor}>
                      <div className={styles.orange}></div>
                      <div className={styles.green}></div>
                      <div className={styles.violet}></div>
                      <div className={styles.blue}></div>
                    </div>
                  </div>
                </div>
              </Grid>
            );
          })}
        </Grid>
      </div>
    </div>
  );
}
export default HoSoVaCV;
