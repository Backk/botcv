const inputs = [
  { title: "Tên công việc: ", placeholder: "Tên công việc", name: "workName" },
  { title: "Tên công ty: ", placeholder: "Tên công ty", name: "companyName" },
  {
    title: "Địa chỉ làm việc: ",
    placeholder: "Địa chỉ làm việc",
    name: "workAddress",
  },
  {
    title: "Hạn nộp hồ sơ: ",
    type: "date",
    style: { width: "200px", paddingRight: "10px" },
    name: "deadline",
  },
  { title: "Mức lương: ", placeholder: "Mức lương", name: "salary" },
  {
    title: "Hình thức làm việc: ",
    placeholder: "Hình thức làm việc",
    name: "hinhThucLamViec",
  },
  { title: "Giới tính: ", placeholder: "Tên công việc", name: "sex" },
  { title: "Số lượng tuyển: ", placeholder: "Tên công việc", name: "amount" },
  { title: "Cấp bậc: ", placeholder: "Cấp bậc", name: "level" },
  { title: "Kinh nghiệm: ", placeholder: "Kinh nghiệm", name: "exp" },
];

export default inputs