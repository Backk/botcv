import { useLocation } from "react-router-dom";
import { useEffect, useState } from "react";
import styles from "./style.module.css";
import CheckIcon from "@mui/icons-material/Check";
import DownloadIcon from "@mui/icons-material/Download";
import apiUrl from "../../../apis/index";
import axios from "axios";
import Button from "../../../components/Button/button";

function ListCv() {
  const location = useLocation();
  const cv = location.state.post.Cv;
  const [fetching, setFetching] = useState(false);

  const sendEmail = async (cv) => {
    setFetching(true);
    try {
      const res = await axios.post(apiUrl.SEND_EMAIL, { receiver: cv.sender });
      if (res.data.success === true) {
        setFetching(false);
        alert("Gửi thành công");
      }
    } catch (err) {
      console.log(err);
      setFetching(false);
      alert("Gửi không thành công");
    }
  };

  useEffect(() => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  }, []);

  return (
    <div className={styles.container}>
      <h3 style={{ color: "#00b14f" }}>{location.state.post.workName}</h3>
      {cv.map((c, index) => {
        return (
          <div key={index} className={styles.cv}>
            <p>{c?.sender}</p>
            <div className={styles.general}>
              <a
                target="__blank"
                className={styles.dowloadBtn}
                href={c?.url}
                download
              >
                <DownloadIcon sx={{ marginRight: "5px" }} /> Dowload Cv
              </a>
              <Button
                title={
                  <CheckIcon
                    sx={{
                      color: "white",
                    }}
                  />
                }
                fetching={fetching}
                onClick={() => sendEmail(c)}
                _styles={{
                  height: "30px",
                  width: "30px",
                  borderRadius: "50%",
                  marginLeft: "20px",
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              />
            </div>
          </div>
        );
      })}
    </div>
  );
}

export default ListCv;
