import styles from "./style.module.css";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import { AuthContext } from "../../../context/authContext";
import { useContext, useEffect, useState } from "react";
import apiUrl from "../../../apis/index";
import Loading from "../../../components/Loading/loading";

const ListPost = () => {
  const navigate = useNavigate();
  const [fetching, setFetching] = useState(false);
  const {
    authState: { user },
  } = useContext(AuthContext);
  const [posts, setPosts] = useState([]);

  const getListPost = async () => {
    setFetching(true);
    try {
      const res = await axios.post(apiUrl.GET_POSTS, { userId: user._id });
      setFetching(false);
      setPosts(res.data.posts);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    user?._id && getListPost();
  }, [user]);

  //console.log(posts);
  return (
    <div className={styles.container}>
      <h3
        style={{
          width: "60%",
          marginLeft: "10px",
        }}
      >
        Các bài đăng chưa được duyệt:
      </h3>
      {fetching === true ? (
        <Loading />
      ) : (
        <>
          {posts.length === 0 ? (
            <h1>Bạn chưa đăng bài nào</h1>
          ) : (
            <div>
              {posts.reverse().map((post, index) => {
                return (
                  <div
                    key={index}
                    className={styles.post}
                    onClick={() =>
                      navigate(`/my-list-post/${post._id}`, {
                        state: { post },
                      })
                    }
                  >
                    <h2
                      style={{
                        color: "#00b14f",
                        fontSize: "1em",
                        width: "60%",
                      }}
                    >
                      {post?.workName}
                    </h2>
                    <p>CV: {post?.Cv.length}</p>
                    <p>Hạn nộp hồ sơ: {post?.deadline}</p>
                  </div>
                );
              })}
            </div>
          )}
        </>
      )}
    </div>
  );
};

export default ListPost;
