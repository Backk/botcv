import React, { useState, useContext } from "react";
import styles from "./style.module.css";
import Input from "../../../components/Input/input";
import { Editor } from "react-draft-wysiwyg";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import { convertToHTML } from "draft-convert";
import DOMPurify from "dompurify";
import Button from "../../../components/Button/button";
import axios from "axios";
import apiUrl from "../../../apis/index";
import inputs from "../constant";
import { useNavigate } from "react-router-dom";
import { AuthContext } from "../../../context/authContext";

function CreatePost() {
  const navigate = useNavigate();
  const {
    authState: { user },
  } = useContext(AuthContext);
  const [editorState, setEditorState] = useState(null);
  // const [editorState, setEditorState] = useState(() =>
  //   EditorState.createEmpty()
  // );
  const [convertedContent, setConvertedContent] = useState(null);
  const [payload, setPlayload] = useState({});
  const convertToHtml = (editorState) => {
    setEditorState(editorState);
    const converted = convertToHTML(editorState.getCurrentContent());
    setConvertedContent(converted);
  };
  const [fetching, setFetChing] = useState(false);

  const createMarkup = (convertedContent) => {
    return {
      __html: DOMPurify.sanitize(convertedContent),
    };
  };

  const handleInputChange = (e) => {
    setPlayload({ ...payload, [e.target.name]: e.target.value });
  };

  const handleSubmit = async () => {
    setFetChing(true);
    const _payload = {
      ...payload,
      detailInfo: createMarkup(convertedContent).__html,
      creatorId: user._id,
    };
    try {
      const res = await axios.post(apiUrl.CREATE_POST, _payload);
      console.log(res.data);
      if (res.data.success === true) {
        setFetChing(false);
        alert(res.data.message);
        navigate(`/post/${res.data.post._id}`, {
          state: { post: res.data.post },
        });
      }
    } catch (err) {
      console.log(err);
      setFetChing(false);
    }
  };

  return (
    <div>
      <div className={styles.container}>
        <h3 style={{ fontWeight: "bold" }}>Thông tin chung: </h3>
        {inputs.map((e, index) => {
          return (
            <div key={index}>
              <p>{e.title}</p>
              <Input
                placeholder={e?.placeholder}
                type={e?.type}
                style={e?.style}
                onChange={(e) => handleInputChange(e)}
                name={e?.name}
              />
            </div>
          );
        })}

        <h3 style={{ marginTop: "20px", fontWeight: "bold" }}>
          Thông tin chi tiết:
        </h3>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            minHeight: "500px",
          }}
        >
          <div style={{ width: "100%" }}>
            <Editor
              editorState={editorState}
              onEditorStateChange={(e) => convertToHtml(e)}
              toolbarStyle={{ height: "10.2%" }}
            />
          </div>
        </div>
        <Button
          onClick={handleSubmit}
          _styles={{ marginTop: "10px" }}
          title={"Tạo bài tuyển dụng"}
          fetching={fetching}
        />
      </div>
    </div>
  );
}

export default CreatePost;
