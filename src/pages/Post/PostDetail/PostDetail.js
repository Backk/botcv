import { useParams } from "react-router-dom";
import styles from "./style.module.css";
import images from "../../../assets/images/images";
import SendIcon from "@mui/icons-material/Send";
import FavoriteIcon from "@mui/icons-material/Favorite";
import { FavoriteBorderOutlined } from "@mui/icons-material";
import { useState, useContext, useEffect } from "react";
import SendCvModal from "./uploadModal";
import { useLocation } from "react-router-dom";
import { AuthContext } from "../../../context/authContext";

function PostDetail() {
  const { postId } = useParams();
  const location = useLocation();
  const [openModal, setOpenModal] = useState(false);
  const post = location.state.post;
  const [favorite, setFavorite] = useState(false);
  const {
    authState: { isAuthenticated, user },
  } = useContext(AuthContext);

  //console.log(post);
  const handleOpanModal = () => {
    if (isAuthenticated === false) {
      alert("Đăng nhập đi bro!!");
      return;
    }

    if (post.creatorId === user._id) {
      alert("Bạn không thể tự ứng tuyển chính mình!!");
      return;
    }

    setOpenModal(true);
  };

  useEffect(() => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  }, []);

  return (
    <div>
      <div
        style={{
          backgroundColor: "#f0f0f0",
          minHeight: "100vh",
          paddingTop: "50px",
        }}
      >
        <div className={styles.titleContainer}>
          <img
            width={"10%"}
            style={{
              border: "1px solid gray",
              borderRadius: "50%",
              marginLeft: "20px",
            }}
            src={images.HR}
          />
          <div className={styles.titleInfo}>
            <h2 style={{ color: "#00b14f", fontSize: 22 }}>{post?.workName}</h2>
            <h3 style={{ fontSize: 14 }}>
              {post.companyName || "TRUNG TÂM CNTT MOBIFONE"}
            </h3>
            <p>Hạn nộp hồ sơ: {post?.deadline || "30/11/2022"}</p>
          </div>
          <div style={{ marginRight: "20px" }}>
            <button className={styles.btn} onClick={() => handleOpanModal()}>
              <SendIcon sx={{ marginRight: "10px" }} /> Ứng tuyển ngay
            </button>
            <button
              className={styles.btn}
              onClick={() => setFavorite(!favorite)}
              style={{
                border: "1px solid #00b14f",
                backgroundColor: favorite ? "#00b14f" : "white",
              }}
            >
              {favorite === true ? (
                <FavoriteIcon
                  sx={{
                    marginRight: "10px",
                  }}
                />
              ) : (
                <FavoriteBorderOutlined
                  sx={{
                    marginRight: "10px",
                    color: "#00b14f",
                  }}
                />
              )}
              {favorite ? (
                "Đã lưu"
              ) : (
                <p style={{ color: "#00b14f" }}>Lưu tin</p>
              )}
            </button>
          </div>
        </div>
        <div className={styles.bodyContainer}>
          <h2 style={{ borderLeft: "7px solid #00b14f", paddingLeft: "8px" }}>
            Chi tiết tuyển dụng
          </h2>
          <div className={styles.box}>
            <p className={styles.headerText}>Thông tin chung</p>
            <div className={styles.boxMain}>
              <div className={styles.boxItem}>
                <strong>Mức lương</strong>
                <span>{post?.salary}</span>
              </div>
              <div className={styles.boxItem}>
                <strong>Hình thức làm việc</strong>
                <span>{post?.hinhThucLamViec}</span>
              </div>
              <div className={styles.boxItem}>
                <strong>Giới tính</strong>
                <span>{post?.sex}</span>
              </div>
              <div className={styles.boxItem}>
                <strong>Số lượng tuyển</strong>
                <span>{post?.amount}</span>
              </div>
              <div className={styles.boxItem}>
                <strong>Cấp bậc</strong>
                <span>{post?.level}</span>
              </div>
              <div className={styles.boxItem}>
                <strong>Kinh nghiệm</strong>
                <span>{post?.exp}</span>
              </div>
            </div>
          </div>

          <div className={styles.box}>
            <p className={styles.headerText}>Địa điểm làm việc</p>
            <div style={{ paddingBottom: 14 }}>- {post?.workAddress}</div>
          </div>

          <div className={styles.box}>
            <p className={styles.headerText}>Thông tin chi tiêt</p>
            <div
              dangerouslySetInnerHTML={{
                __html: post?.detailInfo,
              }}
            ></div>
          </div>
        </div>
      </div>
      <SendCvModal
        postId={postId}
        open={openModal}
        onClose={() => setOpenModal(false)}
      />
    </div>
  );
}

export default PostDetail;
