import styles from "./style.module.css";
import { Modal } from "@mui/material";
import { useState, useEffect, useContext } from "react";
import Backdrop from "@mui/material/Backdrop";
import UploadIcon from "@mui/icons-material/Upload";
import {
  getStorage,
  ref,
  uploadBytesResumable,
  getDownloadURL,
} from "firebase/storage";
import { AuthContext } from "../../../context/authContext";
import Button from "../../../components/Button/button";
import axios from "axios";
import apiUrl from "../../../apis/index";

const SendCvModal = ({ open, onClose, postId }) => {
  const {
    authState: { user },
  } = useContext(AuthContext);
  const [file, setFile] = useState(null);
  const [dowloadUrl, setDowloadUrl] = useState(null);
  const [fetching, setFetching] = useState(false);

  useEffect(() => {
    if (file !== null && file?.name.split(".").pop() !== "pdf") {
      alert("Chỉ tải lên file có định dạng pdf");
      setFile(null);
    }
  }, [file]);

  const uploadToFirebase = () => {
    setFetching(true);
    const storage = getStorage();

    /** @type {any} */
    const metadata = {
      contentType: "application/pdf",
    };

    const storageRef = ref(storage, "cvs/" + file.name);
    const uploadTask = uploadBytesResumable(storageRef, file, metadata);

    uploadTask.on(
      "state_changed",
      (snapshot) => {
        const progress =
          (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        console.log("Upload is " + progress + "% done");
        switch (snapshot.state) {
          case "paused":
            console.log("Upload is paused");
            break;
          case "running":
            console.log("Upload is running");
            break;
        }
      },
      (error) => {
        switch (error.code) {
          case "storage/unauthorized":
            break;
          case "storage/canceled":
            break;

          // ...

          case "storage/unknown":
            break;
        }
      },
      async () => {
        const _dowloadUrl = await getDownloadURL(uploadTask.snapshot.ref);

        const res = await axios.post(apiUrl.SEND_CV, {
          Cv_Url: _dowloadUrl,
          postId,
          sender: user.email,
        });
        if (res.data.success === true) {
          setFetching(false);
          alert(res.data.message);
          setFile(null);
          onClose();
        }
      }
    );
  };

  return (
    <Modal
      open={open}
      onClose={() => {
        onClose();
        setFile(null);
      }}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500,
      }}
      sx={{
        border: "1px soliod red",
      }}
    >
      <div className={styles.modal}>
        <h3>
          Ứng tuyển{" "}
          <strong style={{ color: "#00b14f" }}>
            Chuyên Viên Quản Trị Cơ Sở Dữ Liệu (Database Administator)
          </strong>
        </h3>
        <label className={styles.inputFile} for="fileInput">
          <UploadIcon sx={{ marginRight: "5px" }} />{" "}
          {file === null ? "Tải lên Cv từ máy tính" : file.name}
        </label>
        <input
          onChange={(e) => setFile(e.target.files[0])}
          style={{ display: "none" }}
          type={"file"}
          id="fileInput"
        />
        <Button
          title={"Nộp Cv"}
          _styles={{ backgroundColor: file === null ? "gray" : "" }}
          fetching={fetching}
          onClick={() => uploadToFirebase()}
        />
      </div>
    </Modal>
  );
};

export default SendCvModal;
