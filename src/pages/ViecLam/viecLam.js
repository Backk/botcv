import styles from "./style.module.css";
import SearchBox from "../../components/SearchBox/searchBox";
import Chip from "@mui/material/Chip";
import images from "../../assets/images/images";
import axios from "axios";
import apiUrl from "../../apis/index";
import { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useMediaQuery } from "@mui/material";
import Loading from "../../components/Loading/loading";
import { PostContext } from "../../context/postContext";

const chips = [
  "Tất cả",
  "Hà Nội",
  "Thành phố Hồ Chí Minh",
  "Miền Bắc",
  "Miền Nam",
];

function ViecLam() {
  const [posts, setPosts] = useState([]);
  const navigate = useNavigate();
  const is1750px = useMediaQuery("(max-width:1750px)");
  const [page, setPage] = useState(1);
  const [fetching, setFetching] = useState(false);
  const [numberDot, setNumberDot] = useState([]);
  const { existPost, setExistPost } = useContext(PostContext);
  const [itemPerPage, setItemPerpage] = useState(15);
  //console.log(posts);

  const getPostList = async () => {
    if (existPost.length > 0) {
      const _posts = existPost.filter((e, index) => index === page - 1);
      // console.log(_posts);
      if (_posts.length > 0) {
        setPosts(_posts[0].posts);
        setNumberDot(_posts[0].num_pages);
        return;
      }
    }
    setFetching(true);
    try {
      const res = await axios.post(apiUrl.GET_ALL_POST, {
        page: page,
        num_per_page: itemPerPage,
      });
      setFetching(false);
      setPosts(res.data.data);

      const dots = [];
      for (let i = 0; i < res.data.count / itemPerPage; i++) {
        dots.push(i);
      }
      setExistPost((pre) => [
        ...pre,
        {
          posts: res.data.data,
          num_pages: dots,
        },
      ]);

      setNumberDot(dots);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    window.scrollTo({ top: 0, behavior: "smooth" });
    getPostList();
  }, [page]);

  return (
    <div style={{ backgroundColor: "#f0f0f0" }}>
      <SearchBox />
      <div
        style={{
          minHeight: "80vh",
        }}
      >
        <div className={styles.container}>
          <div className={styles.header}>
            <h3>Tin tuyển dụng, việc làm</h3>
            <button className={styles.showAllBtn}>Xem tất cả -{">"}</button>
          </div>
          <div style={{ paddingLeft: "10px" }}>
            {chips.map((chip, index) => {
              return (
                <Chip
                  sx={{
                    marginRight: "10px",
                    cursor: "pointer",
                    color: "white",
                    backgroundColor: chip === "Tất cả" ? "#00b14f" : "black",
                  }}
                  label={chip}
                  key={index}
                />
              );
            })}
          </div>
          {fetching === true ? (
            <Loading />
          ) : (
            <div
              style={{
                backgroundColor: "white",
                position: "relative",
                minHeight: "67.5vh",
              }}
            >
              <div className={styles.workList}>
                {posts.map((post, index) => {
                  if (is1750px && index === 14) return;
                  return (
                    <div
                      className={styles.workInfo}
                      key={index}
                      onClick={() =>
                        navigate(`/post/${post._id}`, {
                          state: { post },
                        })
                      }
                    >
                      <div className={styles.top}>
                        <img
                          src={images.HR}
                          width={"50px"}
                          style={{
                            border: "1px solid #f0f0f0",
                            height: "50px",
                            margin: "0 10px",
                          }}
                          alt="hr"
                        />
                        <div className={styles.right}>
                          <div className={styles.text}>{post.workName}</div>
                          <div
                            className={styles.text}
                            style={{ color: "gray", fontWeight: "lighter" }}
                          >
                            {post.companyName || "Công ty FPT"}
                          </div>
                        </div>
                      </div>
                      <div className={styles.bottom}>
                        <Chip
                          sx={{ height: "25px", marginRight: "10px" }}
                          label={post.salary}
                        />
                        <Chip
                          sx={{ height: "25px", maxWidth: "200px" }}
                          label={post.workAddress}
                        />
                      </div>
                    </div>
                  );
                })}
              </div>
              <div
                className={styles.pagination}
                style={{
                  bottom: is1750px ? -25 : -20,
                }}
              >
                {numberDot.map((e, index) => {
                  return (
                    <div
                      onClick={() => setPage(index + 1)}
                      key={e}
                      className={styles.dot}
                      style={{
                        backgroundColor:
                          page - 1 === index ? "#00b14f" : "#8a8a8a",
                      }}
                    ></div>
                  );
                })}
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}

export default ViecLam;
