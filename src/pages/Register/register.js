import React, { useState, useRef, useContext } from "react";
import { useNavigate } from "react-router-dom";
import styles from "./style.module.css";
import EmailSharpIcon from "@mui/icons-material/EmailSharp";
import KeySharpIcon from "@mui/icons-material/KeySharp";
import RemoveRedEyeSharpIcon from "@mui/icons-material/RemoveRedEyeSharp";
import VisibilityOffSharpIcon from "@mui/icons-material/VisibilityOffSharp";
import images from "../../assets/images/images";
import { Link } from "react-router-dom";
import { AuthContext } from "../../context/authContext";
import Button from "../../components/Button/button";
import check from "../../utils/checkLogin";

function Register() {
  const { register } = useContext(AuthContext);
  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);
  const [fetching, setFetChing] = useState(false);
  const emailRef = useRef();
  const passwordRef = useRef();
  const confirmPasswordRef = useRef();
  const navigate = useNavigate();

  const handleLogin = async () => {
    if (check(emailRef, passwordRef, confirmPasswordRef) === false) {
      alert("Sai email hoặc mật khẩu");
      return;
    }
    setFetChing(true);
    const payload = {
      email: emailRef.current.value,
      password: passwordRef.current.value,
      confirmPassword: confirmPasswordRef.current.value,
    };
    console.log(payload);

    try {
      const res = await register(payload);
      //console.log(res);
      if (res.success === true) {
        setFetChing(false);
        navigate("/");
      } else {
        alert(res.message);
      }
    } catch (err) {
      console.log(err);
      setFetChing(false);
      alert("Đăng kí không thành công");
    }
  };
  return (
    <div className={styles.container}>
      <div className={styles.leftSide}>
        <div style={{ cursor: "pointer" }} onClick={() => navigate("/")}>
          <img
            width={"100px"}
            style={{ marginBottom: "30px" }}
            src={images.Logo}
          />
          <img
            width={"100px"}
            style={{ marginBottom: "30px" }}
            src={images.Logo}
          />
          <img
            width={"100px"}
            style={{ marginBottom: "30px" }}
            src={images.Logo}
          />
        </div>
        <div style={{width:'50%'}}>
          <span>Email</span>
          <div className={styles.emailInput}>
            <EmailSharpIcon
              sx={{
                color: "#00b14f",
                marginRight: "10px",
              }}
            />
            <input style={{width:'90%'}} placeholder="Nhập email của bạn" ref={emailRef} />
          </div>
          <span>Mật khẩu</span>
          <div className={styles.passwordInput}>
            <KeySharpIcon
              sx={{
                color: "#00b14f",
                marginRight: "10px",
              }}
            />
            <input
              placeholder="Nhập mật khẩu"
              type={showPassword ? "text" : "password"}
              ref={passwordRef}
              style={{width:'83%'}}
            />
            {showPassword === true && (
              <button
                className={styles.showPassword}
                onClick={() => setShowPassword(false)}
              >
                <RemoveRedEyeSharpIcon />
              </button>
            )}
            {showPassword === false && (
              <button
                className={styles.showPassword}
                onClick={() => setShowPassword(true)}
              >
                <VisibilityOffSharpIcon />
              </button>
            )}
          </div>
          <span>Xác nhận mật khẩu</span>
          <div className={styles.passwordInput}>
            <KeySharpIcon
              sx={{
                color: "#00b14f",
                marginRight: "10px",
              }}
            />
            <input
              placeholder="Nhập lại mật khẩu"
              type={showConfirmPassword ? "text" : "password"}
              ref={confirmPasswordRef}
              style={{width:'83%'}}
            />
            {showConfirmPassword === true && (
              <button
                className={styles.showPassword}
                onClick={() => setShowConfirmPassword(false)}
              >
                <RemoveRedEyeSharpIcon />
              </button>
            )}
            {showConfirmPassword === false && (
              <button
                className={styles.showPassword}
                onClick={() => setShowConfirmPassword(true)}
              >
                <VisibilityOffSharpIcon />
              </button>
            )}
          </div>
          <Button
            title={"Đăng kí"}
            fetching={fetching}
            onClick={() => handleLogin()}
          />
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <p>
              Bạn đã có tài khoản? <Link to={"/login"}>Đăng nhập ngay</Link>{" "}
            </p>
          </div>
        </div>
      </div>
      <img src={images.LoginImage1} style={{ height: "100%", width: "50%" }} />
    </div>
  );
}

export default Register;
