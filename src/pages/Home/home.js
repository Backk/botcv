import images from "../../assets/images/images";
import { useEffect } from "react";
import styles from "./style.module.css";

function Home() {
  useEffect(() => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  }, []);

  return (
    <div className={styles.container}>
      <img src={images.Home} className={styles.homeImage} />
      <h2 className={styles.homeSlogan}>
        Đăng tin tuyển dụng miễn phí, tìm CV ứng viên và hơn thế nữa
      </h2>
      <button className={styles.homeBtn}>Tìm hiểu ngay</button>
    </div>
  );
}

export default Home;
