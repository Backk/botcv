export const template = [
  {
    img: "https://vnr500.com.vn/Content/Prospect500/images/dn-banner.jpg",
    item1: "NGÂN HÀNG TMCP VN THỊNH VƯỢNG (VPBANK)",
    item2:
      " VPBank tiếp tục theo đuổi mục tiêu khẳng định vị thế của ngân hàng trên thị trường, đó là nằm trong nhóm 5 Ngân hàng TMCP tư nhân và nhóm 3 Ngân hàng TMCP tư nhân bán lẻ hàng đầu về quy mô cho vay khách hàng, huy động khách hàng và lợi nhuận và chú trọng tăng trưởng chất lượng hoạt động.Những thành quả đạt được trong giai đoạn...",
  },
  {
    img: "https://vnr500.com.vn/Content/Prospect500/images/dn-banner.jpg",
    item1: "CÔNG TY CP TẬP ĐOÀN T&T",
    item2:
      "Công ty TNHH T&T được thành lập năm 1993, là tiền thân của Tập đoàn T&T. Năm 2007, công ty chuyển đổi loại hình doanh nghiệp từ công ty TNHH sang Công ty CP Tập đoàn T&T. Được biết đến là đơn vị sở hữu CLB bóng đá Hà Nội T&T (nay là CLB Bóng đá Hà Nội), thực tế, đằng sau thương hiệu T&T Group là một tập đoàn kinh tế đa ngành có bề dày ...",
  },
  {
    img: "https://vnr500.com.vn/Content/Prospect500/images/dn-banner.jpg",
    item1: "CÔNG TY CP HÀNG KHÔNG VIETJET",
    item2:
      "Công ty CP Hàng không VietJet (Vietjet Aviation JSC), hoạt động với tên VietJet Air, là hãng hàng không tư nhân đầu tiên của Việt Nam, có trụ sở chính tại Sân bay Quốc tế Tân Sơn Nhất TP. Hồ Chí Minh và chi nhánh tại Sân bay Quốc tế Nội Bài Hà Nội. Hãng được Bộ trưởng Bộ Tài chính Việt Nam phê duyệt cấp giấy phép vào tháng 11/2007 cổ phiếu của Công ty...",
  },
  {
    img: "https://vnr500.com.vn/Content/Prospect500/images/dn-banner.jpg",
    item1: "CÔNG TY CP THẾ GIỚI SỐ",
    item2:
      "Thành lập năm 1997, Digiworld − Nhà phân phối và cung cấp dịch vụ Phát triển Thị trường tiên phong hàng đầu Việt Nam, đã đạt được nhiều thành tựu, đặc biệt trong lĩnh vực Công nghệ (ICT). Qua 2 thập kỷ tạo dựng uy tín, đến nay Digiworld đã được 30 thương hiệu công nghệ nổi tiếng thế giới lựa chọn là nhà cung cấp dịch vụ phát triển thị trường...",
  },
  {
    img: "https://vnr500.com.vn/Content/Prospect500/images/dn-banner.jpg",
    item1: "CÔNG TY CP TÔN ĐÔNG Á",
    item2:
      "Công ty Cổ phần Tôn Đông Á được thành lập ngày 5/11/1998 với tên gọi Công ty TNHH Đông Á, sau đó đổi thành Công ty TNHH Tôn Đông Á vào năm 2004. Năm 2009 được xem là một bước ngoặt lớn của Tôn Đông Á khi chuyển đổi từ hình thức công ty TNHH sang công ty cổ phần để phù hợp với tình hình mới, hòa nhập vào sự phát triển chung của đất nước và ...",
  },
  {
    img: "https://vnr500.com.vn/Content/Prospect500/images/dn-banner.jpg",
    item1: "NGÂN HÀNG TMCP Á CHÂU",
    item2:
      "Năm 1993, Ngân hàng TMCP Á Châu (ACB) chính thức được thành lập và đi vào hoạt động. Năm 2014, ACB nâng cấp hệ nghiệp vụ ngân hàng lõi (core banking) từ TCBS lên DNA, thay thế hệ cũ đã sử dụng 14 năm. Hoàn tất việc thay đổi logo, bảng hiệu mặt tiền trụ sở cho toàn bộ các chi nhánh, phòng giao dịch và ATM theo nhận diện thương hiệu mới (công bố ngày 05/01/2015)...",
  },
  {
    img: "https://vnr500.com.vn/Content/Prospect500/images/dn-banner.jpg",
    item1: "TẬP ĐOÀN VINGROUP - CTCP",
    item2:
      "Tiền thân của Vingroup là Tập đoàn Technocom, thành lập năm 1993 tại Ucraina. Đầu những năm 2000, Technocom trở về Việt Nam, tập trung đầu tư vào lĩnh vực du lịch và bất động sản với hai thương hiệu chiến lược ban đầu là Vinpearl và Vincom. Đến tháng 1/2012, công ty CP Vincom và Công ty CP Vinpearl sáp nhập, chính thức hoạt động dưới mô hình ... ",
  },
  {
    img: "https://vnr500.com.vn/Content/Prospect500/images/dn-banner.jpg",
    item1: "CÔNG TY CP ĐẦU TƯ THẾ GIỚI DI ĐỘNG",
    item2:
      "Công ty Cổ phần Đầu tư Thế Giới Di Động (MWG) là nhà bán lẻ số 1 Việt Nam về doanh thu và lợi nhuận, với mạng lưới gần 4.000 cửa hàng trên toàn quốc. MWG vận hành các chuỗi bán lẻ thegioididong.com, Điện Máy Xanh, Bách Hóa Xanh. Ngoài ra, MWG còn mở rộng ra thị trường nước ngoài với chuỗi bán lẻ thiết bị di động và điện máy tại Campuchia. ",
  },
  {
    img: "https://vnr500.com.vn/Content/Prospect500/images/dn-banner.jpg",
    item1: "CÔNG TY CP TẬP ĐOÀN GELEX",
    item2:
      "Công ty Cổ phần Tập đoàn GELEX (GELEX – mã cổ phiếu GEX), tiền thân là Tổng Công ty Thiết bị kỹ thuật điện, được thành lập theo Quyết định số 237 QĐ/CNNg-TCNS ngày 10/07/1990 của Bộ Công nghiệp nặng (nay là Bộ Công Thương), trên cơ sở tập trung các đơn vị sản xuất kinh doanh, dịch vụ, nghiên cứu kỹ thuật điện do Bộ Công nghiệp nặng quản lý...",
  },
  {
    img: "https://vnr500.com.vn/Content/Prospect500/images/dn-banner.jpg",
    item1: "CÔNG TY CP TẬP ĐOÀN MASAN",
    item2:
      "Masan Group là một trong những công ty lớn nhất trong khu vực kinh tế tư nhân Việt Nam, tập trung hoạt động trong ngành hàng tiêu dùng và tài nguyên của Việt Nam. Chúng tôi có nhiều thành tích trong hoạt động xây dựng, mua lại và quản lý các nền tảng hoạt động kinh doanh có quy mô lớn nhằm phát triển và khai thác các tiềm năng dài hạn trong ...",
  },
  {
    img: "https://vnr500.com.vn/Content/Prospect500/images/dn-banner.jpg",
    item1: "CÔNG TY CP TẬP ĐOÀN HÒA PHÁT",
    item2:
      "Hòa Phát là Tập đoàn sản xuất công nghiệp hàng đầu Việt Nam. Khởi đầu từ một Công ty chuyên buôn bán các loại máy xây dựng từ tháng 8/1992, Hòa Phát lần lượt mở rộng sang các lĩnh vực khác như Nội thất, ống thép, thép xây dựng, điện lạnh, bất động sản và nông nghiệp. Ngày 15/11/2007, Hòa Phát chính thức niêm yết cổ phiếu trên thị trường chứng khoán ...",
  },
  {
    img: "https://vnr500.com.vn/Content/Prospect500/images/dn-banner.jpg",
    item1: "CÔNG TY CP TẬP ĐOÀN TRƯỜNG HẢI",
    item2:
      "Hoạt động kinh doanh chính của Thaco hiện nay là sản xuất - lắp ráp - phân phối, cung ứng dịch vụ bảo trì sửa chữa và phụ tùng ôtô: bao gồm sản xuất và kinh doanh xe thương mại (xe tải và xe bus); Sản xuất và kinh doanh xe du lịch các thương hiệu: Kia (Hàn Quốc), Mazda (Nhật Bản), Peugeot (Châu Âu). Hệ thống phân phối gồm 93 showroom và 59 đại lý trải dài trên toàn quốc... ",
  },
  {
    img: "https://vnr500.com.vn/Content/Prospect500/images/dn-banner.jpg",
    item1: "NGÂN HÀNG TMCP QUỐC TẾ VIỆT NAM",
    item2:
      "Với nguồn lực tài chính vững mạnh, chiến lược cụ thể và tiên phong chuyển đổi sâu rộng, VIB là ngân hàng duy nhất ở Việt Nam có tỷ trọng bán lẻ chiếm đến 90% cơ cấu danh mục tín dụng– một tỷ lệ vượt trội theo đánh giá của các chuyên gia trong ngành. Trong đó gần 95% dư nợ bán lẻ có tài sản đảm bảo. Với dư nợ bán lẻ tăng trưởng bình quân trên 50%/năm và... ",
  },
  {
    img: "https://vnr500.com.vn/Content/Prospect500/images/dn-banner.jpg",
    item1: "CÔNG TY CP TẬP ĐOÀN HOA SEN",
    item2:
      "Công ty CP Tập đoàn Hoa Sen tiền thân là Công ty CP Hoa Sen thành lập vào ngày 08/08/2001 với vốn điều lệ ban đầu là 30 tỷ đồng. Ngày 08/11/2007, công ty đổi thành Công ty CP Tập đoàn Hoa Sen (Hoa Sen Group). Là doanh nghiệp có vốn ngoài quốc doanh đầu tiên ở Việt Nam xây dựng nhà máy sản xuất thép cán nguội. Trong bối cảnh nền kinh tế còn nhiều khó khăn,...",
  },
  {
    img: "https://vnr500.com.vn/Content/Prospect500/images/dn-banner.jpg",
    item1: "CÔNG TY CP FPT",
    item2:
      "Ngày 13/9/1988, Công ty Công nghệ Thực phẩm (The Food Processing Technology Company), tiền thân của Công ty FPT được thành lập. Ngày 27/10/1990, công ty đổi tên thành Công ty Phát triển Đầu tư Công nghệ FPT. Tháng 3/2002, công ty cổ phần hóa với tên Công ty Cổ phần Phát triển Đầu tư Công nghệ FPT. Công ty bắt đầu niêm yết với mã FPT ngày 13/12/2006, trên sàn giao dịch ...",
  },
  // {
  //   name: "",
  //   img: "",
  //   item1: "",
  //   item2: "",
  // },
];
