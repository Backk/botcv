import styles from "./style.module.css";
import Grid from "@mui/material/Grid";
import { template } from "./Template";
import { Link } from "react-router-dom";

function ListCompany() {
  return (
    <div className={styles.container}>
      <div className={styles.boxSearch}>
        <ul className={styles.navHeader}>
          <li className={styles.navItem}>
            <Link className={styles.navActive} to={"/list-company"}>
              Danh sách công ty
            </Link>
          </li>
          <li className={styles.navItem}>
            <Link className={styles.nav} to={"/list-top-company"}>
              Top công ty
            </Link>
          </li>
        </ul>
        <div>
          <h1 style={{ color: "#00b14f", marginBottom: 20, marginTop: 50 }}>
            Khám phá các công ty nổi bật
          </h1>
          <p style={{ opacity: 0.8, marginBottom: 40 }}>
            Tìm kiếm các công ty phù hợp với bản thân
          </p>
        </div>
        <form className={styles.formSearch}>
          <input className={styles.Input} placeholder="Nhập tên công ty" />
          <button className={styles.btnSearch}>Tìm kiếm</button>
        </form>
      </div>
      <div className={styles.main}>
        <Grid container spacing={5}>
          {template.map((templates, index) => {
            return (
              <Grid item md={4} sm={6}>
                <div
                  className={styles.item}
                  style={{
                    // marginLeft: "12.5%",
                    marginTop: 10,
                    // marginRight: "12.5%",
                  }}
                >
                  <div className={styles.boxCompany}>
                    <div className={styles.templateImage}>
                      <img
                        width={"100%"}
                        height={"100%"}
                        src={templates.img}
                        alt=""
                      ></img>
                    </div>
                    <div className={styles.templateItemInfo}>
                      <a href="comming-soon" className={styles.templateItem}>
                        {templates.item1}
                      </a>

                      <p className={styles.templateItem2}>{templates.item2}</p>
                    </div>
                  </div>
                </div>
              </Grid>
            );
          })}
        </Grid>
      </div>
    </div>
  );
}

export default ListCompany;
