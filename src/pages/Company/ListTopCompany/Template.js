export const template = [
  {
    img: "https://vnr500.com.vn/Content/Prospect500/images/dn-banner.jpg",
    item: "https://www.topcv.vn/top-cong-ty-vien-thong-tl17.html",
    item1: "TOP CÔNG TY VIỄN THÔNG",
    item2:
      " Tất cả những việc làm hot nhất tại các Công ty viễn thông hàng đầu Việt Nam. Nơi bạn sẽ được trải nghiệm, thử thách bản thân. Các Công ty viễn thông lọt vào danh sách này dựa trên những tiêu chí Chất lượng sản phẩm - Môi trường làm việc - Chế độ đãi ngộ - Khả năng học hỏi do TopCV đánh giá, để đảm bảo rằng đây...",
  },
  {
    img: "https://vnr500.com.vn/Content/Prospect500/images/dn-banner.jpg",
    item1: "TOP CÔNG TY THƯƠNG MẠI ĐIỆN TỬ",
    item2:
      "Tất cả những việc làm hot nhất tại các Công ty thương mại điện tử hàng đầu Việt Nam. Nơi bạn sẽ được trải nghiệm, thử thách bản thân. Các Công ty thương mại điện tử lọt vào danh sách này dựa trên những tiêu chí Chất lượng sản phẩm - Môi trường làm việc - Chế độ đãi ngộ - Khả năng học hỏi do TopCV đánh giá, để...",
  },
  {
    img: "https://vnr500.com.vn/Content/Prospect500/images/dn-banner.jpg",
    item1: "TOP CÔNG TY SẢN XUẤT HÀNG TIÊU DÙNG NHANH ( FMCG)",
    item2:
      "Tất cả những việc làm hot nhất tại các Công ty FMCG hàng đầu Việt Nam. Nơi bạn sẽ được trải nghiệm, thử thách bản thân. Các Công ty FMCG lọt vào danh sách này dựa trên những tiêu chí Chất lượng sản phẩm - Môi trường làm việc - Chế độ đãi ngộ - Khả năng học hỏi do TopCV đánh giá, để đảm bảo rằng đây là nơi...",
  },
  {
    img: "https://vnr500.com.vn/Content/Prospect500/images/dn-banner.jpg",
    item1: "TOP CÔNG TY FINTECH",
    item2:
      "Tất cả những việc làm hot nhất tại các Công ty FINTECH hàng đầu Việt Nam. Các Công ty FINTECH lọt vào danh sách này dựa trên những tiêu chí Chất lượng sản phẩm - Môi trường làm việc - Chế độ đãi ngộ - Khả năng học hỏi do TopCV đánh giá, để đảm bảo rằng đây là nơi làm việc tốt nhất dành cho bạn.",
  },
  {
    img: "https://vnr500.com.vn/Content/Prospect500/images/dn-banner.jpg",
    item1: "TOP CÔNG TY EDTECH",
    item2:
      "Tất cả những việc làm hot nhất tại các Công ty EDTECH hàng đầu Việt Nam. Các Công ty EDTECH lọt vào danh sách này dựa trên những tiêu chí Chất lượng sản phẩm - Môi trường làm việc - Chế độ đãi ngộ - Khả năng học hỏi do TopCV đánh giá, để đảm bảo rằng đây là nơi làm việc tốt nhất dành cho bạn.",
  },
  {
    img: "https://vnr500.com.vn/Content/Prospect500/images/dn-banner.jpg",
    item1: "TOP CÔNG TY DU LỊCH",
    item2:
      "Tất cả những việc làm hot nhất tại các Công ty Du lịch hàng đầu Việt Nam. Các Công ty Du lịch lọt vào danh sách này dựa trên những tiêu chí Chất lượng sản phẩm - Môi trường làm việc - Chế độ đãi ngộ - Khả năng học hỏi do TopCV đánh giá, để đảm bảo rằng đây là nơi làm việc tốt nhất dành cho bạn.",
  },
  {
    img: "https://vnr500.com.vn/Content/Prospect500/images/dn-banner.jpg",
    item1: "TOP CÔNG TY THIẾT KẾ ĐỒ HỌA",
    item2:
      "Tất cả những việc làm hot nhất tại các Công ty Thiết kế đồ họa hàng đầu Việt Nam. Nơi bạn sẽ được trải nghiệm, thử thách bản thân, cùng đóng góp và tạo dựng những giá trị to lớn cho xã hội. Các Công ty Thiết kế đồ họa lọt vào danh sách này dựa trên những tiêu chí Chất lượng sản phẩm - Môi trường làm việc -...",
  },
  {
    img: "https://vnr500.com.vn/Content/Prospect500/images/dn-banner.jpg",
    item1: "TOP CÔNG TY BẤT ĐỘNG SẢN",
    item2:
      "Tất cả những việc làm hot nhất tại các Công ty Bất động sản hàng đầu Việt Nam. Các Công ty Bất động sản lọt vào danh sách này dựa trên những tiêu chí Chất lượng sản phẩm - Môi trường làm việc - Chế độ đãi ngộ - Khả năng học hỏi do TopCV đánh giá, để đảm bảo rằng đây là nơi làm việc tốt nhất dành cho...",
  },
  {
    img: "https://vnr500.com.vn/Content/Prospect500/images/dn-banner.jpg",
    item1: "TOP CÔNG TY BÁN LẺ",
    item2:
      "Tất cả những việc làm hot nhất tại các Công ty Bán lẻ hàng đầu Việt Nam. Các Công ty bán lẻ lọt vào danh sách này dựa trên những tiêu chí Chất lượng sản phẩm - Môi trường làm việc - Chế độ đãi ngộ - Khả năng học hỏi do TopCV đánh giá, để đảm bảo rằng đây là nơi làm việc tốt nhất dành cho bạn.",
  },
  // {
  //   name: "",
  //   img: "",
  //   item1: "",
  //   item2: "",
  // },
];
