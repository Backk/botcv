import React, { useState, useRef, useContext } from "react";
import { useNavigate } from "react-router-dom";
import styles from "./style.module.css";
import EmailSharpIcon from "@mui/icons-material/EmailSharp";
import KeySharpIcon from "@mui/icons-material/KeySharp";
import RemoveRedEyeSharpIcon from "@mui/icons-material/RemoveRedEyeSharp";
import VisibilityOffSharpIcon from "@mui/icons-material/VisibilityOffSharp";
import images from "../../assets/images/images";
import { Link } from "react-router-dom";
import { AuthContext } from "../../context/authContext";
import { useMediaQuery } from "@mui/material";
import Button from "../../components/Button/button";
import check from "../../utils/checkLogin";

function Login() {
  const [showPassword, setShowPassword] = useState(false);
  const [fetching, setFetChing] = useState(false);
  const emailRef = useRef();
  const passwordRef = useRef();
  const navigate = useNavigate();
  const { login } = useContext(AuthContext);
  const is1440px = useMediaQuery("(min-width:1650px)");

  const handleLogin = async () => {
    if (check(emailRef, passwordRef, null) === false) {
      alert("Sai email hoặc mật khẩu");
      return;
    }

    setFetChing(true);
    const payload = {
      email: emailRef.current.value,
      password: passwordRef.current.value,
    };
    try {
      const res = await login(payload);
      //console.log(res);
      if (res.success === true) {
        setFetChing(false);
        navigate("/");
      } else {
        alert(res.message);
      }
    } catch (err) {
      console.log(err);
      setFetChing(false);
      alert("Đăng nhập không thành công");
    }
  };

  return (
    <div className={styles.container}>
      <div className={styles.leftSide}>
        <div
          style={{
            cursor: "pointer",
            display: "flex",
            justifyContent: "center",
          }}
          onClick={() => navigate("/")}
        >
          <img
            width={"100px"}
            style={{ marginBottom: "30px" }}
            src={images.Logo}
          />
          <img
            width={"100px"}
            style={{ marginBottom: "30px" }}
            src={images.Logo}
          />
          <img
            width={"100px"}
            style={{ marginBottom: "30px" }}
            src={images.Logo}
          />
        </div>
        <div style={{width:'50%'}}>
          <span style={{ paddingBottom: "10px" }}>Email</span>
          <div className={styles.emailInput}>
            <EmailSharpIcon
              sx={{
                color: "#00b14f",
                marginRight: "10px",
              }}
            />
            <input style={{width:'90%'}} placeholder="Nhập email của bạn" ref={emailRef} />
          </div>
          <span>Mật khẩu</span>
          <div className={styles.passwordInput}>
            <KeySharpIcon
              sx={{
                color: "#00b14f",
                marginRight: "10px",
              }}
            />
            <input
              placeholder="Nhập mật khẩu"
              type={showPassword ? "text" : "password"}
              ref={passwordRef}
              style={{width:'83%'}}
            />
            {showPassword === true && (
              <button
                className={styles.showPassword}
                onClick={() => setShowPassword(false)}
              >
                <RemoveRedEyeSharpIcon />
              </button>
            )}
            {showPassword === false && (
              <button
                className={styles.showPassword}
                onClick={() => setShowPassword(true)}
              >
                <VisibilityOffSharpIcon />
              </button>
            )}
          </div>
          <Button
            title={"Đăng nhập"}
            onClick={handleLogin}
            fetching={fetching}
          />
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              flexDirection: "column",
            }}
          >
            <p>
              Bạn chưa có tài khoản? <Link to={"/register"}>Đăng kí ngay</Link>{" "}
            </p>
            <Link to={"/comming-soon"}>Quên mật khẩu</Link>
          </div>
        </div>
      </div>
      <img src={images.LoginImage1} style={{ height: "100%", width: "50%" }} />
    </div>
  );
}

export default Login;
