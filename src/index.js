import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import reportWebVitals from "./reportWebVitals";
import router from "./router/router";
import { RouterProvider } from "react-router-dom";
import AuthContextProvider from "./context/authContext";
import PostContextProvider from "./context/postContext";

// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyCNt297H4T6cNmlxX2w4D4GL12rRnAol_M",
  authDomain: "botcv-deeaf.firebaseapp.com",
  projectId: "botcv-deeaf",
  storageBucket: "botcv-deeaf.appspot.com",
  messagingSenderId: "215210150728",
  appId: "1:215210150728:web:2a89bcbf4f82b60e49417d",
  measurementId: "G-18CQDW6NPF",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);

const root = ReactDOM.createRoot(document.getElementById("root"));

root.render(
  <AuthContextProvider>
    <PostContextProvider>
      <RouterProvider router={router} />
    </PostContextProvider>
  </AuthContextProvider>
);
reportWebVitals();
