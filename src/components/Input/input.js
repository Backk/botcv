import styles from './style.module.css';

function Input(props) {
  return <input className={styles.inputs} {...props} />;
}

export default Input;
