import { CircularProgress } from "@mui/material";

function Loading() {
  return (
    <div
      style={{
        width: "100%",
        height: "100%",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <CircularProgress size={30} sx={{ color: "#00b14f", margin:'200px auto' }} />
    </div>
  );
}

export default Loading;
