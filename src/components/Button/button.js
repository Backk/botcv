import { CircularProgress } from "@mui/material";

function Button({ title, _styles, fetching, ...props }) {
  return (
    <button
      style={{
        cursor: "pointer",
        fontWeight: "bold",
        color: "white",
        ..._styles,
      }}
      {...props}
    >
      {fetching === true ? (
        <CircularProgress size={20} sx={{ color: "white" }} />
      ) : (
        title
      )}
    </button>
  );
}

export default Button;
