import styles from "./style.module.css";
import { Select, MenuItem } from "@mui/material";
import { useState } from "react";

const searInput = [
  {
    placeholder: "Tên công việc",
    name: "workName",
    type: "input",
    data: [
      {
        label: "Tất cả ngành nghề",
        value: "all",
      },
      {
        label: "Kinh doanh",
        value: "marketing",
      },
      {
        label: "Phiên dịch",
        value: "translator",
      },
    ],
  },
  {
    placeholder: "Ngành nghề",
    name: "career",
    data: [
      {
        label: "Tất cả ngành nghề",
        value: "all",
      },
      {
        label: "Kinh doanh",
        value: "marketing",
      },
      {
        label: "Phiên dịch",
        value: "translator",
      },
    ],
  },
  {
    placeholder: "Lĩnh vực",
    name: "field",
    data: [
      {
        label: "Tất cả lĩnh vực công ty",
        value: "all",
      },
      {
        label: "Kinh doanh",
        value: "marketing",
      },
      {
        label: "Phiên dịch",
        value: "translator",
      },
    ],
  },
  {
    placeholder: "Địa điểm",
    name: "address",
    data: [
      {
        label: "Tất cả Địa điểm",
        value: "all",
      },
      {
        label: "Kinh doanh",
        value: "marketing",
      },
      {
        label: "Phiên dịch",
        value: "translator",
      },
    ],
  },
];

const SearchBox = () => {
  const [career, setCareer] = useState("all");

  const handleChange = (e) => {
    setCareer(e.target.value);
    console.log(e.target.name);
    console.log(e.target.value);
  };
  return (
    <div
      style={{
        display: "flex",
        padding: "15px",
        justifyContent: "center",
        alignItems: "center",
        maxWidth: "100%",
        margin: "0 auto",
        paddingTop: '20px',
        backgroundColor: 'white',
        paddingLeft:'11%',
        paddingRight: '11%',
        marginBottom:'10px'
      }}
    >
      {searInput.map((ele, index) => {
        if (ele.type === "input") {
          return (
            <input
              key={index}
              placeholder={ele?.placeholder}
              name={ele?.name}
              className={styles.inputSearch}
            />
          );
        }
        return (
          <Select
            sx={{ height: "44px", marginRight: "10px", width: "15%" }}
            value={career}
            name={ele?.name}
            onChange={handleChange}
            key={ele.name}
          >
            {ele.data &&
              ele?.data?.map((field, index) => {
                return (
                  <MenuItem key={field.label} value={field.value}>
                    {field.label}
                  </MenuItem>
                );
              })}
          </Select>
        );
      })}
      <button className={styles.btn}>Tìm kiếm</button>
    </div>
  );
};

export default SearchBox;
