import styles from "./style.module.css";

const items = [
  {
    title: "Về BotCv",
    links: ["Giới thiệu", "Tuyển dụng", "Liên hệ", "Hỏi đáp"],
  },
  {
    title: "Đối tác",
    links: ["TestCenter", "TopHR", "ViecNgay", "Happy Time"],
  },
  {
    title: "Hồ sơ và CV",
    links: [
      "Quản lí CV của bạn",
      "TopCV Profile",
      "Hướng dẫn viết CV",
      "Review CV",
    ],
  },
  {
    title: "Xây dựng sự nghiệp",
    links: [
      "Việc làm tốt nhất",
      "Việc làm lương cao",
      "Việc làm quản lí",
      "Việc làm từ xa",
    ],
  },
];

function AppFooter() {
  return (
      <div className={styles.container}>
        {items.map((item, index) => {
          return (
            <div key={index} className={styles.item}>
              <p className={styles.title}>{item.title}</p>
              <div>
                {item.links.map((link, index) => {
                  return <p key={link}>{link}</p>;
                })}
              </div>
            </div>
          );
        })}
      </div>
  );
}

export default AppFooter;
