const links = [
  {
    text: "Việc làm",
    to: "viec-lam",
    subLinks: [
      {
        text: "Tìm việc làm",
        to: "find-work",
      },
      {
        text: "Việc làm đã ứng tuyển",
        to: "find-work",
      },
      {
        text: "Việc làm đã lưu",
        to: "find-work",
      },
      {
        text: "Việc làm phù hợp",
        to: "find-work",
      },
    ],
  },
  {
    text: "Hồ sơ và CV",
    to: "/ho-so-va-cong-viec",
    subLinks: [
      {
        text: "Quản lí CV",
        to: "/manage-cv",
      },
      {
        text: "Quản lí Cover Letter",
        to: "CV-maneger",
      },
      {
        text: "Mẫu CV",
        to: "/ho-so-va-cong-viec",
      },
      {
        text: "Mẫu Cover Letter",
        to: "CV-maneger",
      },
      {
        text: "Dịch vụ tư vấn CV",
        to: "CV-maneger",
      },
      {
        text: "Hướng dẫn viết CV theo ngành nghề",
        to: "CV-maneger",
      },
      {
        text: "BotCv Profile",
        to: "CV-maneger",
      },
    ],
  },
  {
    text: "Công ty",
    to: "list-company",
    subLinks: [
      {
        text: "Danh sách công ty",
        to: "list-company",
      },
      {
        text: "Top công ty",
        to: "list-top-company",
      },
    ],
  },
  {
    text: "Phát triển sự nghiệp",
    to: "comming-soon",
    subLinks: [
      {
        text: "BotCV Contest",
        to: "contest",
      },
      {
        text: "BotCV Skills",
        to: "contest",
      },
      {
        text: "Trắc nghiệm tính cách MBTI",
        to: "contest",
      },
      {
        text: "Trắc nghiệm MI",
        to: "contest",
      },
    ],
  },
  {
    text: "Công cụ",
    to: "comming-soon",
    subLinks: [
      {
        text: "BotCV Contest",
        to: "contest",
      },
      {
        text: "BotCV Skills",
        to: "contest",
      },
      {
        text: "Trắc nghiệm tính cách MBTI",
        to: "contest",
      },
      {
        text: "Trắc nghiệm MI",
        to: "contest",
      },
    ],
  },
];

export default links;
