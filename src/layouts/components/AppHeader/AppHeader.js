import { Link } from "react-router-dom";
import styles from "./style.module.css";
import { useContext, useState } from "react";
import { AuthContext } from "../../../context/authContext";
import images from "../../../assets/images/images";
import ForumIcon from "@mui/icons-material/Forum";
import NotificationsNoneIcon from "@mui/icons-material/NotificationsNone";
import { Collapse, List, ListItemButton, ListItemText } from "@mui/material";
import { useNavigate, NavLink } from "react-router-dom";
import CrisisAlertIcon from "@mui/icons-material/CrisisAlert";
import links from "./links";

function AppHeader() {
  const {
    authState: { isAuthenticated, user },
    logout,
  } = useContext(AuthContext);

  const navigate = useNavigate();

  const _logout = async () => {
    navigate("/");
    window.location.reload();
    await logout();
  };
  const [open, setOpen] = useState(false);
  //console.log(authState);
  return (
    <div className={styles.container}>
      <div className={styles.leftSide}>
        <Link to={"/"}>
          <img width={"70px"} src={images.Logo} alt="logo" />
        </Link>
        {links.map((e, index) => {
          return (
            <div key={e.text} className={styles.leftSideItem}>
              <NavLink
                to={e.to}
                className={({ isActive, isPending }) =>
                  !isActive ? styles.route : styles.routeActive
                }
                style={{ padding: "10px" }}
              >
                {e.text}
              </NavLink>
              <div
                className={styles.subLinkContainer}
                style={{ display: "flex", flexDirection: "column", top: 60 }}
              >
                {e?.subLinks?.map((subLink) => {
                  return (
                    <NavLink to={subLink.to} className={styles.subLink}>
                      <CrisisAlertIcon
                        sx={{ marginRight: "10px", color: "#00b14f" }}
                      />
                      {subLink.text}
                    </NavLink>
                  );
                })}
              </div>
            </div>
          );
        })}
      </div>
      {isAuthenticated === false && (
        <div className={styles.rightSide}>
          <Link className={styles.loginBtn} to={"/login"}>
            Đăng nhập
          </Link>
          <Link className={styles.registerBtn} to={"/register"}>
            Đăng kí
          </Link>
        </div>
      )}
      {isAuthenticated === true && (
        <div className={styles.rightSide} style={{ cursor: "pointer" }}>
          <ForumIcon
            sx={{ fontSize: "30px", color: "#00b14f", marginRight: "20px" }}
          />
          <NotificationsNoneIcon
            sx={{ fontSize: "30px", color: "#00b14f", marginRight: "20px" }}
          />
          <div
            style={{
              position: "relative",
            }}
            className={styles.leftSideItem}
          >
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <img src={images.User} style={{width: 50, borderRadius: 50}} alt="user_photo" />
              <p style={{ marginLeft: "10px", fontSize: "15px" }}>
                {user && user.email}
              </p>
            </div>
            <div
              className={styles.subLinkContainer}
              style={{
                display: "flex",
                flexDirection: "column",
                top: 50,
                width: "200px",
              }}
            >
              <NavLink to={"/post/create"} className={styles.subLink}>
                Đăng bài
              </NavLink>
              <NavLink to={"/my-list-post"} className={styles.subLink}>
                Duyệt bài
              </NavLink>
              <NavLink onClick={() => _logout()} className={styles.subLink}>
                Đăng xuất
              </NavLink>
            </div>
          </div>
          {/* <KeyboardArrowDownIcon sx={{marginLeft: '10px', fontSize:'30px'}}/> */}
        </div>
      )}
    </div>
  );
}

export default AppHeader;
