const regex =
  /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

const check = (email, password, confirmPassword) => {
  if (
    email.current.value === "" ||
    password.current.value === "" ||
    confirmPassword?.current.value === ""
  )
    return false;

  if (confirmPassword) {
    if (password.current.value !== confirmPassword.current.value) {
      return false;
    }
  }

  if (!email.current.value.match(regex)) return false;

  return true;
};

export default check;
