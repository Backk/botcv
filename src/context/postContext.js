import React, { createContext, useState, useEffect } from "react";

export const PostContext = createContext({});

const PostContextProvider = ({ children }) => {
  const [existPost, setExistPost] = useState([]);

  const value = { existPost, setExistPost };
  return <PostContext.Provider value={value}>{children}</PostContext.Provider>;
};

export default PostContextProvider;
