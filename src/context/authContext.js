import React, { createContext, useState, useEffect } from "react";
import apiUrl from "../apis/index";
import axios from "axios";

export const AuthContext = createContext({});

const AuthContextProvider = ({ children }) => {
  const [authState, setAuthState] = useState({
    user: null,
    isAuthenticated: false,
  });

  useEffect(() => {
    loadUser();
  }, []);

  const login = async (payload) => {
    try {
      const res = await axios.post(apiUrl.LOGIN_URL, payload);
      if (res.data.success === true) {
        const authState = {
          user: res.data.user,
          isAuthenticated: true,
        };
        setAuthState(authState);
        localStorage.setItem("user", JSON.stringify(authState));
      }
      return res.data;
    } catch (err) {
      console.log(err);
    }
  };

  const register = async (payload) => {
    try {
      const res = await axios.post(apiUrl.REGISTER_URL, payload);
      if (res.data.success === true) {
        const authState = {
          user: res.data.user,
          isAuthenticated: true,
        };
        setAuthState(authState);
        localStorage.setItem("user", JSON.stringify(authState));
      }
      return res.data;
    } catch (err) {
      console.log(err);
    }
  };

  const logout = async () => {
    console.log("logout");
    localStorage.removeItem("user");
    loadUser();
  };

  const loadUser = () => {
    const authState = localStorage.getItem("user");
    if (authState) {
      setAuthState(JSON.parse(authState));
    } else {
      setAuthState({
        user: null,
        isAuthenticated: false,
      });
    }
  };
  const value = { authState, loadUser, login, logout, register };
  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
};

export default AuthContextProvider;
