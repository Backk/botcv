import { createBrowserRouter } from "react-router-dom";
import App from "../App";
import Home from "../pages/Home/home";
import Login from "../pages/Login/login";
import Register from "../pages/Register/register";
import CommingSoon from "../pages/CommingSoon";
import CreatePost from "../pages/Post/Create/CreatePost";
import PostDetail from "../pages/Post/PostDetail/PostDetail";
import ListPost from "../pages/Post/PostConfirm/listPost";
import ListCv from "../pages/Post/PostConfirm/listCv";
import ViecLam from "../pages/ViecLam/viecLam";
import HoSoVaCV from "../pages/HoSoVaCV/HoSoVaCV";
import ManageCV from "../pages/ManageCV/manageCV";
import ListCompany from "../pages/Company/ListCompany/ListCompany";
import ListTopCompany from "../pages/Company/ListTopCompany/ListTopCompany";

const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
    children: [
      {
        path: "/",
        element: <Home />,
      },
      {
        path: "/comming-soon",
        element: <CommingSoon />,
      },
      {
        path: "post/create",
        element: <CreatePost />,
      },
      {
        path: "post/:postId",
        element: <PostDetail />,
      },
      {
        path: "/my-list-post",
        element: <ListPost />,
      },
      {
        path: "/my-list-post/:postId",
        element: <ListCv />,
      },

      {
        path: "/viec-lam",
        element: <ViecLam />,
      },
      {
        path: "/ho-so-va-cong-viec",
        element: <HoSoVaCV />,
      },
      {
        path: "/manage-cv",
        element: <ManageCV />,
      },
      {
        path: "list-company",
        element: <ListCompany />,
      },
      {
        path: "list-top-company",
        element: <ListTopCompany />,
      },
    ],
  },
  {
    path: "/login",
    element: <Login />,
  },
  {
    path: "/register",
    element: <Register />,
  },
]);

export default router;
